
from sqlalchemy.ext.declarative import declarative_base, declared_attr
from sqlalchemy.ext.declarative import has_inherited_table

from sqlalchemy import Column, Integer, String, Boolean,DateTime,Date,ForeignKey,Numeric,UniqueConstraint, Float
from huduma.utils import timestamp_uuid


def CharField(max_length,*args,**kwargs):
    return Column(String(max_length),*args,**kwargs)

def DateTimeField(*args,**kwargs):
    return Column(DateTime,*args,**kwargs)

def DateField(*args,**kwargs):
    return Column(Date,*args,**kwargs)
 

def BooleanField(*args,**kwargs):
    return Column(Boolean,*args,**kwargs)

def IntegerField(*args,**kwargs):
    return Column(Integer,*args,**kwargs)

def ForeignKeyField(references,*args,**kwargs):
    return CharField(50,ForeignKey(references),*args, **kwargs)

def TenantField(**kwargs):
    return CharField(50, nullable=False,**kwargs)

def DecimalField(max_digits,decimal_places,*args,**kwargs):
    return Column(Numeric(precision = max_digits, scale = decimal_places),*args,**kwargs)

def FloatField(*args,**kwargs):
    return Column(Float,*args,**kwargs)

def UniqueTogether(*args,**kwargs):
    return UniqueConstraint(*args,**kwargs)

def PrimaryKeyField():
    return CharField(50, primary_key=True, default=timestamp_uuid)

class CRUDMixin:

    @classmethod
    def all(cls):
        return cls.__table__.select()
    
    @classmethod
    def insert(cls):
        return cls.__table__.insert()

    @classmethod
    def delete(cls):
        return cls.__table__.delete()
    
    @classmethod
    def update(cls):
        return cls.__table__.update()
    
    @classmethod
    def get(cls, pk):
        return cls.all().where(cls.id == pk)

        

    

class BaseTable(CRUDMixin):
    id = PrimaryKeyField()
    
    @declared_attr
    def __tablename__(cls):
        name = cls.__make_name()
        return name.lower() + 's'
        
    @classmethod
    def __make_char(cls,index,char):
        if char.isupper() and index != 0:
            return '_' + char.lower()
        
        return char
    
    @classmethod
    def __make_name(cls):
        new_string_l = []
        current_name = cls.__name__

        if current_name.isupper():
            #this is one name that is upper all
            return current_name

        for index,char in enumerate(current_name):
            new_char = cls.__make_char(index,char)

            new_string_l.append(new_char)
        

        new_string = ''.join(new_string_l)

        return new_string

Base = declarative_base(cls = BaseTable)