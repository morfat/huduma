class CoreMiddleWare:
    
    def __init__(self,db_engine):
        self.db_engine = db_engine

   
    def process_request(self,req,resp):
        req.context['conn'] = self.db_engine.connect()


    def process_resource(self,req,resp,resource,params):
        req.context['transaction'] = req.context['conn'].begin()


    def process_response(self,req,resp,resource,req_succeeded):
        try:
            if req_succeeded:
                req.context['transaction'].commit()
            else:
                req.context['transaction'].rollback()
        except:
            pass
        finally:
            req.context['conn'].close()
