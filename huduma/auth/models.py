from huduma import models

class User(models.Base):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    pasword =  models.CharField(max_length=100)
    username = models.CharField(max_length=100, unique=True)
    age =      models.IntegerField()