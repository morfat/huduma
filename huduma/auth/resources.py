
from .models import User

from huduma.resources import ResourceCollection


class UserCollection(ResourceCollection):

    model_class = User
    selected_columns = [ User.id, User.first_name ]
    searchable_fields = ["first_name", "last_name"]
    filterable_fields = ["username", "id","first_name"]

        