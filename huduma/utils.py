import uuid
import datetime

def timestamp_uuid():
    return ( str( datetime.datetime.utcnow().timestamp() ).split('.')[0] ) + uuid.uuid4().hex