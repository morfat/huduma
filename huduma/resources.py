from sqlalchemy import select
from sqlalchemy.sql import and_, or_
from sqlalchemy import column


from huduma.paginators import CursorPaginator

class Resource:
    """ Base class for all resource classes """

    model_class = None
    selected_columns = None
    PK_COLUMN_NAME = 'id'



    def get_queryset(self, *args, **kwargs):
        if self.selected_columns:
            return select( self.selected_columns )
        return select( self.model_class )
    
    def __get_column_filter(self,col_name,col_condition,col_value):
        #example input => ('name','contains','mosoti',)

        col = column( col_name ) #to sqlalchemy column

        ops_dict = {
            'startswith':col.startswith(col_value),
            'endswith': col.endswith(col_value),
            'contains': col.contains(col_value),
            'eq': col.op("=")(col_value),
            'lt': col.op("<")(col_value),
            'lte': col.op("<=")(col_value),
            'gt': col.op(">")(col_value),
            'gte': col.op(">=")(col_value)
        }   
    
        return ops_dict[col_condition]
    

    def __get_col_filters(self, **filters):
        """ for the filters params we expect example dict:
           { 'name__startswith': ' mosoti' , 'age': 20, 'gender__ne':'M' } e.t.c.
        """

        col_filters = []

        for field_and_condition, field_value in filters.items():
            field__condition = field_and_condition.split("__")
            
            field_name = field__condition[0]
            condition = 'eq' #by default we expect this

            try:
                condition = field__condition[1]
            except IndexError:
                pass
            
            col_filters.append( self.__get_column_filter(field_name , condition ,field_value) )
        return col_filters

 

    def filter_queryset(self, queryset, search_by={}, filter_by={}, order_by=None):
        #print (search_by, filter_by)

        #1. Apply filters
        return queryset.where(
            and_(
                or_(
                    *self.__get_col_filters(**search_by)
                ),
                *self.__get_col_filters(**filter_by)
            )
        )
    
    def execute_queryset(self, conn, queryset, *args, **kwargs):
        return conn.execute(queryset)
    
    def fetch_all(self, conn, queryset, limit=None, for_update=False):
        """ executed db. and gets results """
        if limit:
            queryset = queryset.limit(limit)
        
        if for_update:
            queryset = queryset.with_for_update()
        
        return self.execute_queryset(conn,queryset).fetchall()

    
    def find(self, filters, order_by=None):

        """ 
        find in the resource queryset . mostsly used in code or from other classes calls.
        Returns queryset
        """

        queryset = self.get_queryset()

        return self.filter_queryset(queryset, filter_by=filters, order_by=order_by, limit=limit)
    
    def find_and_fetch_all(self,conn, filters, order_by=None, limit=None, for_update=False):
        filtered_queryset =  self.find(filters,order_by)
        return self.fetch_all(conn, filtered_queryset,limit,for_update)
    
    def insert_one(self,conn, data, model_class=None, *args, **kwargs):
        if model_class is None:
            model_class = self.model_class
        
        queryset = model_class.insert().values(data)

        inserted = self.execute_queryset(conn, queryset, *args, **kwargs )

        return { self.PK_COLUMN_NAME: inserted.inserted_primary_key[0], **data }

        



    
   


class ResourceCollection(Resource):

    """ to handle creating and listing of resoruces """

    searchable_fields = None
    filterable_fields = None

    SEARCH_QUERY_PARAM_NAME = 'search'
    paginator_object = CursorPaginator()


        
    def get_valid_search_params(self, search_text):
        search_params = {}
        if search_text:
            for field in self.searchable_fields:
                field__condition = "{field}__contains".format(field = field)
                search_params.update({ field__condition: search_text })
        
        return search_params
    
    def get_valid_filter_params(self, query_params):
        filter_params = {}

        for field_and_condition, field_value in query_params.items():
            field_name = field_and_condition.split("__")[0] #e.g get 'name' from 'name__startswith'

            if field_name in self.filterable_fields:
                filter_params.update( {field_and_condition: field_value} )
        
        return filter_params
    
 
    def on_get(self, req, resp, *args, **kwargs):
        queryset = self.get_queryset()
        conn = req.context['conn']

        query_params = req.params

        #filter by search or filters
        search_text = query_params.pop(self.SEARCH_QUERY_PARAM_NAME, None)
        search_params = self.get_valid_search_params(search_text)
        filter_params = self.get_valid_filter_params(query_params)


        filtered_queryset = self.filter_queryset(queryset, search_by=search_params, filter_by=filter_params)

        #apply pagination
        paginated = self.paginator_object.paginate_queryset(req, filtered_queryset, pk_column_name=self.PK_COLUMN_NAME)
   
        #fetch paginated
        resultset = self.fetch_all(conn, queryset=paginated.get("queryset"))
        results = [ dict(r) for r in resultset ]

        #get pagination object
        pagination = self.paginator_object.get_pagination(url=req.uri, results=results, before=paginated.get("cursor_before"),
                        after=paginated.get("cursor_after"), page_number=paginated.get("page_number"),
                        page_size=paginated.get("page_size"),pk_column_name=self.PK_COLUMN_NAME
                        )

        resp.media = { "data": results, "pagination": pagination }
    

    def on_post(self,req, resp, *args, **kwargs):
        conn = req.context['conn']
        inserted = self.insert_one(conn, data = req.media )
        resp.media = {"data": [ inserted ] }




